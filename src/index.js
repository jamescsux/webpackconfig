import { run } from "./app/app";
import { AlertService } from "./app/alert.service";
import "./main.scss";
import "./bg.scss";

import { ComponentService } from "./app/component.service";

const alertService = new AlertService();
const componentService = new ComponentService();
run(alertService, componentService);

const age = "12";
